function setSlickSlider (elem, param) {
	let slick = elem.slick({
		dots:            param.dots,
		draggable:       param.draggable,
		arrows:          param.arrows,
		prevArrow:       param.prevArrow,
		nextArrow:       param.nextArrow,
		infinite:        param.infinite,
		slidesToShow:    param.slidesToShow,
		slidesToScroll:  param.slidesToScroll,
		responsive:      param.responsive,
		swipe:           param.swipe,
		swipeToSlide:    param.swipeToSlide,
		verticalSwiping: param.verticalSwiping,
		vertical:        param.vertical,
		autoplay:        param.autoplay,
		autoplaySpeed:   param.autoplaySpeed,
		variableWidth:   param.variableWidth,
		fade:            param.fade,
		fadeSpeed:       param.fadeSpeed,
		centerMode:      param.centerMode,
		focusOnSelect:   param.focusOnSelect
	});

	return slick;
}

function setSlick () {
	setSlickSlider($('#bannerSlider'), {
		slidesToShow:  1,
		arrows:        false,
		infinite:      true,
		fade:          true,
		autoplay:      false,
		autoplaySpeed: 10000,
		prevArrow:     $('.bannerPrev'),
		nextArrow:     $('.bannerNext'),
		dots:          true
	})
	setSlickSlider($('#slotsSlider'), {
		slidesToShow:   6,
		arrows:         true,
		slidesToScroll: 7,
		infinite:       false,
		prevArrow:      $('.slotsSliderPrev'),
		nextArrow:      $('.slotsSliderNext'),
		responsive:     [{
			breakpoint: 1400,
			settings:   {
				slidesToShow:   6,
				slidesToScroll: 6,
			}
		}, {
			breakpoint: 1200,
			settings:   {
				slidesToShow:   5,
				slidesToScroll: 5,
			}
		}, {
			breakpoint: 991,
			settings:   {
				slidesToShow:   4,
				slidesToScroll: 4,
			}
		},
			{
				breakpoint: 767,
				settings:   {
					slidesToShow:   3,
					slidesToScroll: 3,
				}
			},
			{
				breakpoint: 576,
				settings:   {
					slidesToShow:   2.2,
					slidesToScroll: 2,
				}
			},
			{
				breakpoint: 280,
				settings:   {
					slidesToShow:   1.5,
					slidesToScroll: 1,
				}
			},
		],
	})
	setSlickSlider($('#newSlider'), {
		slidesToShow:   6,
		arrows:         true,
		slidesToScroll: 7,
		infinite:       false,
		prevArrow:      $('.newSliderPrev'),
		nextArrow:      $('.newSliderNext'),
		responsive:     [{
			breakpoint: 1400,
			settings:   {
				slidesToShow:   6,
				slidesToScroll: 6,
			}
		}, {
			breakpoint: 1200,
			settings:   {
				slidesToShow:   5,
				slidesToScroll: 5,
			}
		}, {
			breakpoint: 991,
			settings:   {
				slidesToShow:   4,
				slidesToScroll: 4,
			}
		},
			{
				breakpoint: 767,
				settings:   {
					slidesToShow:   3,
					slidesToScroll: 3,
				}
			},
			{
				breakpoint: 576,
				settings:   {
					slidesToShow:   2.2,
					slidesToScroll: 2,
				}
			},
			{
				breakpoint: 280,
				settings:   {
					slidesToShow:   1.5,
					slidesToScroll: 1,
				}
			},
		],
	})
	setSlickSlider($('#trendingSlider'), {
		slidesToShow:   6,
		arrows:         true,
		slidesToScroll: 7,
		infinite:       false,
		prevArrow:      $('.trendingSliderPrev'),
		nextArrow:      $('.trendingSliderNext'),
		responsive:     [{
			breakpoint: 1400,
			settings:   {
				slidesToShow:   6,
				slidesToScroll: 6,
			}
		}, {
			breakpoint: 1200,
			settings:   {
				slidesToShow:   5,
				slidesToScroll: 5,
			}
		}, {
			breakpoint: 991,
			settings:   {
				slidesToShow:   4,
				slidesToScroll: 4,
			}
		},
			{
				breakpoint: 767,
				settings:   {
					slidesToShow:   3,
					slidesToScroll: 3,
				}
			},
			{
				breakpoint: 576,
				settings:   {
					slidesToShow:   2.2,
					slidesToScroll: 2,
				}
			},
			{
				breakpoint: 280,
				settings:   {
					slidesToShow:   1.5,
					slidesToScroll: 1,
				}
			},
		],
	})
	setSlickSlider($('#baccaratSlider'), {
		slidesToShow:   5,
		arrows:         true,
		slidesToScroll: 5,
		infinite:       false,
		prevArrow:      $('.baccaratSliderPrev'),
		nextArrow:      $('.baccaratSliderNext'),
		responsive:     [{
			breakpoint: 1400,
			settings:   {
				slidesToShow:   4,
				slidesToScroll: 4,
			}
		}, {
			breakpoint: 1200,
			settings:   {
				slidesToShow:   3,
				slidesToScroll: 3,
			}
		}, {
			breakpoint: 991,
			settings:   {
				slidesToShow:   3,
				slidesToScroll: 3,
			}
		},
			{
				breakpoint: 767,
				settings:   {
					slidesToShow:   2,
					slidesToScroll: 2,
				}
			},
			{
				breakpoint: 576,
				settings:   {
					slidesToShow:   1.6,
					slidesToScroll: 2,
				}
			},
			{
				breakpoint: 280,
				settings:   {
					slidesToShow:   1.5,
					slidesToScroll: 1,
				}
			},
		],
	})
	setSlickSlider($('#catSlider'), {
		slidesToShow: 10,
		arrows:       true,
		swipe:        true,
		swipeToSlide: true,
		infinite:     false,
		prevArrow:      $('.categoriesSliderPrev'),
		nextArrow:      $('.categoriesSliderNext'),
		responsive:   [{
			breakpoint: 1400,
			settings:   {
				slidesToShow: 6,
			}
		}, {
			breakpoint: 1200,
			settings:   {
				slidesToShow: 6,
			}
		}, {
			breakpoint: 991,
			settings:   {
				slidesToShow: 5,
			}
		},
			{
				breakpoint: 767,
				settings:   {
					slidesToShow: 4,
				}
			},
			{
				breakpoint: 576,
				settings:   {
					slidesToShow: 3.5,
				}
			},
		],
	})
	setSlickSlider($('#promoSlider'), {
		slidesToShow:  3.5,
		arrows:        false,
		swipe:         true,
		swipeToSlide:  true,
		infinite:      false,
		variableWidth: true,
		responsive:    [{
			breakpoint: 1400,
			settings:   {
				slidesToShow: 3.5,
			}
		}, {
			breakpoint: 1200,
			settings:   {
				slidesToShow: 3.5,
			}
		}, {
			breakpoint: 991,
			settings:   {
				slidesToShow: 2.5,
			}
		},
			{
				breakpoint: 767,
				settings:   {
					slidesToShow: 1.5,
				}
			},
			{
				breakpoint: 576,
				settings:   {
					slidesToShow:  1.5,
					variableWidth: false,
				}
			},
		],
	})
	setSlickSlider($('#providersSlider'), {
		slidesToShow:   8,
		arrows:         true,
		slidesToScroll: 8,
		infinite:       false,
		prevArrow:      $('.providersSliderPrev'),
		nextArrow:      $('.providersSliderNext'),
		responsive:     [{
			breakpoint: 1400,
			settings:   {
				slidesToShow:   6,
				slidesToScroll: 6,
			}
		}, {
			breakpoint: 1200,
			settings:   {
				slidesToShow:   5,
				slidesToScroll: 5,
			}
		}, {
			breakpoint: 991,
			settings:   {
				slidesToShow:   4,
				slidesToScroll: 4,
			}
		},
			{
				breakpoint: 768,
				settings:   {
					slidesToShow:   3,
					slidesToScroll: 3,
				}
			},
			{
				breakpoint: 577,
				settings:   {
					slidesToShow:   2.5,
					slidesToScroll: 2,
				}
			},
			{
				breakpoint: 280,
				settings:   {
					slidesToShow:   1.5,
					slidesToScroll: 1,
				}
			},
		],
	})
}

function setScrollbars () {
	$('.search__result-container').overlayScrollbars({
		className:       "os-theme-dark",
		paddingAbsolute: true,
		scrollbars:      {
			clickScrolling: true
		}
	});
	$('.filter-scroller').overlayScrollbars({
		className:       "os-theme-dark",
		paddingAbsolute: true,
		scrollbars:      {
			clickScrolling: true
		}
	});
	$('.tbl').overlayScrollbars({
		className:       "os-theme-dark",
		paddingAbsolute: true,
		scrollbars:      {
			clickScrolling: true
		}
	});
}


function intTel () {
	var accInput = document.querySelector("#accMobileNumber");
	var depositInput = document.querySelector("#depositMobileNumber");
	var modalInput = document.querySelector("#modalMobileNumber");
	var widgetInput = document.querySelector("#widgetMobileNumber");

	if (accInput){
			window.intlTelInput(accInput, {
			autoPlaceholder:  "on",
			formatOnDisplay: false,
			placeholderNumberType: "MOBILE",
			separateDialCode: true,
			utilsScript:      "assets/vendors/js/utils.js",
		});
	}

	if (widgetInput){
		window.intlTelInput(widgetInput, {
			autoPlaceholder:  "on",
			formatOnDisplay: false,
			placeholderNumberType: "MOBILE",
			separateDialCode: true,
			utilsScript:      "assets/vendors/js/utils.js",
		});
	}

	if (depositInput){
		window.intlTelInput(depositInput, {
			autoPlaceholder:  "on",
			formatOnDisplay: false,
			placeholderNumberType: "MOBILE",
			separateDialCode: true,
			utilsScript:      "assets/vendors/js/utils.js",
		});
	}

	if (modalInput){
		window.intlTelInput(modalInput, {
			autoPlaceholder:  "on",
			formatOnDisplay: false,
			placeholderNumberType: "MOBILE",
			separateDialCode: true,
			utilsScript:      "assets/vendors/js/utils.js",
		});
	}
}

function bsModalBodyFix () {  //prevent body scroll on modal show
	$('.modal').on('hidden.bs.modal', function () {
		if ($(".modal:visible").length > 0) {
			setTimeout(function () {
				$('body').addClass('modal-open');
			}, 200)
		}
	});
}

function favouriteStars () {
	$('.cards__item-favourite').click(function () {
		$(this).toggleClass('active')
	})
}

function sidebar () {
	$('.sidebar__list-li a').click(function () {
		$('.sidebar__list-li a').removeClass('active')
		$(this).addClass('active');
	})
	function close () {
		$('.page').removeClass('active');
		$('.navbar .logo').show();
	}

	function open () {
		$('.page').addClass('active');
		$('.navbar .logo').hide();
	}

	$('.navbar .logo__btn').click(function () {
		open();
	})
	$('.sidebar .logo__btn').click(function () {
		close();
	})

	$('.sidebar-overlay').click(function () {
		close();
	})

	function checkWidth () {
		let windowSize = $(window).width();

		if (windowSize <= 991) {
			close();
		} else {
			open();
		}
	}

	checkWidth();
	$(window).resize(checkWidth);

	$('.sidebar__btn').click(function (e) {
		e.preventDefault();
		$(this).toggleClass('active');
		$(this).parent().parent().find('.sidebar__list-li').stop().slideToggle('fast');
	})

	$('.theme').click(function (e) {
		e.preventDefault();
		$(this).toggleClass('dark');
		if ($(this).hasClass('dark')) {
			$(this).find('label').text('AM');
			$('body').removeClass('dark-mode');
		} else {
			$(this).find('label').text('PM');
			$('body').addClass('dark-mode');
		}
	})

	$('.lang').click(function () {
		$(this).toggleClass('active');
		$(this).find('.sidebar__btn').toggleClass('active');
		$(this).closest('.sidebar').find('.lang-container').slideToggle('fast');
	})
}

function navbar () {
	$('.user__nav-btn').click(function (e) {
		e.preventDefault();
		$(this).parent().find('.user__nav-dropdown').toggle();
	})

	$(".userside-dropdown").click(function (e) {
		e.stopPropagation();
	})
	$(".funds-dropdown").click(function (e) {
		e.stopPropagation();
	})
	$('.search__result').click(function (e) {
		e.stopPropagation();
	})

	$('.filter__btn-placeholder').click(function () {
		let selector = $(this).closest('.filter__btn');

		if (selector.hasClass('active')) {
			selector.removeClass('active');
		} else {
			$('.filter__btn-placeholder').each(function (index, item) {
				$(item).closest('.filter__btn').removeClass('active');
			});
			selector.addClass('active');
		}
	})

	$('.search__result-navbar .cls').click(function (e) {
		e.preventDefault();
		$(this).parent().parent().hide();
	})
	$('.search__input input').on("focus", function (e) {
		e.stopPropagation();
		$(this).parent().parent().find('.search__result').show();
	})

	$('body', 'html').click(function (event) {
		function hider (btn, element) {
			if (btn !== event.target && !btn.has(event.target).length) {
				element.hide();
			}
		}


		hider($(".user__nav-btn"), $(".userside-dropdown"));
		hider($(".user__nav-funds_btn"), $(".funds-dropdown"));
		hider($('.search__input'), $('.search__result'));
	})

	$('#search-sm').click(function (e) {
		e.preventDefault();
		$('.search-sm').toggleClass('active');
	})

	$('.search-sm__header .back-btn').click(function (e) {
		e.preventDefault();
		$('.search-sm').removeClass('active');
	})

	$('.user__nav-search-btn').click(function (e) {
		e.preventDefault();
		$('.search-sm').toggleClass('active');
	})
}

function clickers () {

	$('#oldPromoBtn').click(function () {
		$(this).parent().addClass('loaded');
		$(this).remove();
		$('.promotions-body__old').fadeIn();
	})

	$('.faq-list__header').click(function () {
		let selector = $(this).parent();

		if (selector.hasClass('active')) {
			selector.removeClass('active');
		} else {
			$('.notify__header').each(function (index, item) {
				$(item).parent().removeClass('active');
			});
			selector.addClass('active');
		}
	})

	$('.provider-btn').click(function (e) {
		e.preventDefault();
		$(this).toggleClass('active');
	})

	$('.cards-header__btn').click(function (e) {
		e.preventDefault();
		$(this).toggleClass('active');
		if ($(this).hasClass('active')) {
			$(this).closest('.cards-header').find('.cards-header__dropdown').slideDown('fast');
		} else {
			$(this).closest('.cards-header').find('.cards-header__dropdown').slideUp('fast');
		}
	})

	$('.confirm').click(function (e) {
		e.preventDefault();
		e.stopPropagation();
		$(this).parent().parent().find('.b-form__sms').fadeIn('fast');

		$('.b-form__sms').click(function (e) {
			e.stopPropagation();
		})

		$('body', 'html').click(function () {
			$('.b-form__sms').fadeOut('fast');
		})
	});

	$('.b-form__eye').mousedown(function () {
		$(this).parent().find('input').attr('type', 'text');
		$(this).html('');
		$(this).append('<svg class="icon">\n'+'<use xlink:href="#eye-hidden"></use>\n'+'</svg>')
	}).mouseup(function () {
		$(this).parent().find('input').attr('type', 'password');
		$(this).html('');
		$(this).append('<svg class="icon">\n'+'<use xlink:href="#eye"></use>\n'+'</svg>')
	});

	$('#signUp-password').focusin(function () {
		$(this).parent().parent().find('.password-must').addClass('active');
	}).focusout(function () {
		if ($(this).val().length <= 0){
			$(this).parent().parent().find('.password-must').removeClass('active');
		}
	})

	$('#change-password').focusin(function () {
		$(this).parent().parent().find('.password-must').addClass('active');
	}).focusout(function () {
		if ($(this).val().length <= 0){
			$(this).parent().parent().find('.password-must').removeClass('active');
		}
	})
	$('#confirm-password').focusin(function () {
		$(this).parent().parent().find('.password-must').addClass('active');
	}).focusout(function () {
		if ($(this).val().length <= 0){
			$(this).parent().parent().find('.password-must').removeClass('active');
		}
	})

	var myInput = document.getElementById("confirm-password");
	var myInput = document.getElementById("change-password");
	var myInput = document.getElementById("signUp-password");
	var letter = document.getElementById("letter");
	var capital = document.getElementById("capital");
	var number = document.getElementById("number");
	var length = document.getElementById("length");

	myInput.onkeyup = (function () {
		var lowerCaseLetters = /[a-z]/g;
		if(myInput.value.match(lowerCaseLetters)) {
			letter.classList.remove("invalid");
			letter.classList.add("valid");
		} else {
			letter.classList.remove("valid");
			letter.classList.add("invalid");
		}

		// Validate capital letters
		var upperCaseLetters = /[A-Z]/g;
		if(myInput.value.match(upperCaseLetters)) {
			capital.classList.remove("invalid");
			capital.classList.add("valid");
		} else {
			capital.classList.remove("valid");
			capital.classList.add("invalid");
		}

		// Validate numbers
		var numbers = /[0-9]/g;
		if(myInput.value.match(numbers)) {
			number.classList.remove("invalid");
			number.classList.add("valid");
		} else {
			number.classList.remove("valid");
			number.classList.add("invalid");
		}

		// Validate length
		if(myInput.value.length >= 8) {
			length.classList.remove("invalid");
			length.classList.add("valid");
		} else {
			length.classList.remove("valid");
			length.classList.add("invalid");
		}
	})

	$('#widget-signUp-password').focusin(function () {
		$(this).parent().parent().find('.password-must').addClass('active');
	}).focusout(function () {
		if ($(this).val().length <= 0){
			$(this).parent().parent().find('.password-must').removeClass('active');
		}
	})


	$('.tip-btn').click(function (e) {
		e.stopPropagation();
		$(this).closest('.tip').find('.tip-container').fadeIn('fast');
	});
	$('.tip-container').click(function (e) {
		e.stopPropagation();
	})
	$('body', 'html').click(function () {
		$('.tip-container').fadeOut('fast');
	});

	$('.b-form__amount-btn').click(function () {
		$('.b-form__amount-btn').removeClass('active');
		$(this).addClass('active');
		let amount = $(this).text();
		let amountInt = parseInt(amount, 10);
		$(this).closest('.payment-form').find('input[type=number]').val(amountInt);
	})


	$('.payments__card-header').click(function () {
		let selector = $(this).closest('.payments__card');

		if (selector.hasClass('active')) {
			selector.removeClass('active');
		} else {
			$('.payments__card-header').each(function (index, item) {
				$(item).closest('.payments__card').removeClass('active');
			});
			selector.addClass('active');
		}
	})
}



function userProfile () {
	function tabLocation () { //redirect url to current tab
		let url = location.href.replace(/\/$/, "");

		if (location.hash) {
			const hash = url.split("#");
			$('#profile-tabs a[href="#' + hash[1] + '"]').tab("show");
			url = location.href.replace(/\/#/, "#");
			history.replaceState(null, null, url);
			setTimeout(() => {
				$(window).scrollTop(0);
			}, 400);
		}

		$('a[data-toggle="tab"]').on("click", function () {
			let newUrl;
			const hash = $(this).attr("href");
			if (hash == "#balance") {
				newUrl = url.split("#")[0];
			} else {
				newUrl = url.split("#")[0] + hash;
			}
			newUrl += " ";
			history.replaceState(null, null, newUrl);
		});
	};

	tabLocation();

	$(window).on('hashchange', function () {
		tabLocation();
		if (window.location.hash == '#deposit-now'){
			$('#depositModal').modal('toggle');
		}
		if (window.location.hash == '#verification'){
			$('#kycNeededModal').modal('toggle');
		}
	});

}



function notifications () {
	let delBtn = $('#notifyDelete');


	delBtn.click(function () {
		$('.notify__check input:checked').parent().parent().parent().remove();
		if ($(this).is(":checked") && $('input:checked').length != 0) {
			delBtn.show();
		} else if ($('input:checked').length <=0){
			delBtn.hide();
		}
	})
	$('.notify__check input').change(function () {
		if ($(this).is(":checked") && $('input:checked').length != 0) {
			delBtn.show();
		} else if ($('input:checked').length <=0){
			delBtn.hide();
		}
	});

	$('.notify__header').click(function () {
		let selector = $(this).parent();
		if (selector.hasClass('active')) {
			selector.removeClass('active');
		} else {
			$('.notify__header').each(function (index, item) {
				$(item).parent().removeClass('active');
			});
			selector.addClass('active');
		}
	})
}

function drag () {
	$('.b-form__drag-btn').click(function (e) {
		e.preventDefault();
		$(this).closest('.b-form__drag').find('.b-form__drag-container').click();
	})

	document.querySelectorAll(".b-form__drag-input").forEach((inputElement) => {
		const dropZoneElement = inputElement.closest(".b-form__drag-container");

		dropZoneElement.addEventListener("click", (e) => {
			inputElement.click();
		});

		inputElement.addEventListener("change", (e) => {
			if (inputElement.files.length) {
				updateThumbnail(dropZoneElement, inputElement.files[0]);
			}
		});

		dropZoneElement.addEventListener("dragover", (e) => {
			e.preventDefault();
			dropZoneElement.classList.add("dragging");
		});

		["dragleave", "dragend"].forEach((type) => {
			dropZoneElement.addEventListener(type, (e) => {
				dropZoneElement.classList.remove("dragging");
			});
		});

		dropZoneElement.addEventListener("drop", (e) => {
			e.preventDefault();

			if (e.dataTransfer.files.length) {
				inputElement.files = e.dataTransfer.files;
				updateThumbnail(dropZoneElement, e.dataTransfer.files[0]);
			}

			dropZoneElement.classList.remove("dragging");
			dropZoneElement.classList.add("dragged");
		});

	});
}

function modalTabs () {
	$("*[data-tabbtn]").click(function (e) {
		e.preventDefault();
		let attr = $(this).attr('data-tabbtn');
		$('.b-modal__tab').hide();
		$('.b-modal__tab[data-tab=' + attr + ']').show();

		if (attr === "kyc-verification") {
			$('.b-modal__tab-progress').removeClass('step-1');
			$('.b-modal__tab-progress').addClass('step-2');
		} else if (attr === "cashout") {
			$('.b-modal__tab-progress').removeClass('step-2');
			$('.b-modal__tab-progress').addClass('step-3');
		}
	});
}

function infoTips(){
	$('.infotip').each(function (index, item) {
		$(this).mousemove(function () {
			$(item).find('.infotip-container').fadeIn('fast');
		}).mouseleave(function () {
			$(item).find('.infotip-container').fadeOut('fast');
		})
	});
}

function fullscreen(){
	function startFullscreen () {
		$('.fullscreen-btn').html('');
		$('.fullscreen-btn').append('<svg class="icon">\n'+'<use xlink:href="#fullscreen-close"></use>\n'+'</svg>')
		setTimeout(function () {
			$('.page').removeClass('active');
		}, 200)
		$('.page__content').addClass('game-fullscreen');
	}

	function endFullscreen () {
		$('.fullscreen-btn').html('');
		$('.fullscreen-btn').append('<svg class="icon">\n'+'<use xlink:href="#fullscreen"></use>\n'+'</svg>')
		setTimeout(function () {
			$('.page').addClass('active');
		}, 200)
		$('.page__content').removeClass('game-fullscreen');
	}

	$('.fullscreen-btn').click(function (e) {
		e.preventDefault();
		toggleFullScreen();
	})

	function checkWidth () {
		let windowSize = $(window).width();
		let path = window.location.pathname;
		if (windowSize <= 991 && path === '/game.html') {
			toggleFullScreen();
		}
	}

	checkWidth();

	function toggleFullScreen() {
		if (!document.fullscreenElement &&    // alternative standard method
			!document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
			if (document.documentElement.requestFullscreen) {
				document.documentElement.requestFullscreen();
				startFullscreen ();
			} else if (document.documentElement.msRequestFullscreen) {
				document.documentElement.msRequestFullscreen();
				startFullscreen ();
			} else if (document.documentElement.mozRequestFullScreen) {
				document.documentElement.mozRequestFullScreen();
				startFullscreen ();
			} else if (document.documentElement.webkitRequestFullscreen) {
				document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
				startFullscreen ();
			}
		} else {
			if (document.exitFullscreen) {
				document.exitFullscreen();
				endFullscreen ();
			} else if (document.msExitFullscreen) {
				document.msExitFullscreen();
				endFullscreen ();
			} else if (document.mozCancelFullScreen) {
				document.mozCancelFullScreen();
				endFullscreen ();
			} else if (document.webkitExitFullscreen) {
				document.webkitExitFullscreen();
				endFullscreen ();
			}
		}
	}
}

function widgets(){
	let input = $('#widgetAmount');
	let btn = $('.widget-form__span i');

	let value = input.val();
	btn.text(value);

	input.on("input propertychange",function(){
		if ($(this).val() <= 0){
			btn.text('0$');
		} else{
			btn.text($(this).val() + '$');
		}
	});

	function showWidget () {
		function showFirstStep(){
			$('.widget-form').hide();
			$('.widget-form').eq(0).show();
			$('.page').removeClass('locked');
		}
		showFirstStep();

		$('*[show-widget]').click(function () {
			let attr = $(this).attr('show-widget');
			$('.widget-form').hide();
			$('.widget-form[data-widget=' + attr + ']').show();

			if ($('.widget-form[data-widget=' + attr + ']').index() >= 0){
				$('.page').addClass('locked');
			}
		});

		$('.widget-form__cls').click(function () {
			showFirstStep();
		})

	}

	showWidget ();


	function scrollPusher(){
		let height = $('.banner-section').height();
		$(window).scroll(function () {
			let scroll = $(window).scrollTop();
			if(height <= scroll){
				$('.push-deposit').fadeIn();
			} else{
				$('.push-deposit').fadeOut();
			}
		})
	}

	scrollPusher();
}

function gameInfoTip(){
	let btn = $('.cards__tip');
	let infoTip = $('.gameTip');

	btn.click(function (e) {
		e.stopPropagation();
		let x = $(this).offset().left;
		let y = $(this).offset().top + 20;

		let xOffset = x - infoTip.width() / 2;

		infoTip.css({
			'display' : 'block',
			'top' : y,
			'left' : xOffset,
		})
		$('body', 'html').click(function (event){
			infoTip.fadeOut();
		})
		setTimeout(function () {
			infoTip.fadeOut();
		}, 5000)
	});
}

function rulesDropdown(){
	$('.rules-menu__list-link.toggle').click(function () {
		let selector = $(this).parent().find('.rules-menu__list-sub');
		if (selector.hasClass('active')) {
			selector.removeClass('active');
			selector.stop().slideUp('fast');
		} else {
			$('.rules-menu__list-link.toggle').each(function (index, item) {
				$(item).parent().find('.rules-menu__list-sub').removeClass('active')
				$(item).parent().find('.rules-menu__list-sub').stop().slideUp('fast');
			});
			selector.addClass('active');
			selector.stop().slideDown('fast');
		}
	})
}

$(window).load(function () {
	$('.loader').fadeOut();
})

$(function () {
	setSlick();
	setScrollbars();
	intTel();
	bsModalBodyFix();
	favouriteStars();
	sidebar();
	navbar();
	clickers();
	userProfile();
	notifications();
	drag();
	modalTabs();
	infoTips();
	fullscreen();
	widgets();
	gameInfoTip();
	rulesDropdown();
})
